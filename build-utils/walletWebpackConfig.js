//
// This script is a Webpack plugin used to resolve wallet module in the build process
//
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv
const path = require('path')
const fs = require('fs')
const chalk = require('chalk')

// supported wallets, specify one of these when building the app (case insensitive)
const walletDirs = [
  'IW',
  'Command',
  'Gan',
  'FD'
]

// helper methods for console output
const log = (preText, text, color, bgColor) => console.log(chalk[color][bgColor](' ' + preText + ' ') + ' ' + text)
const logInfo = text => log('INFO [Wallet module build]', text, 'black', 'bgBlue')
const logError = text => log('ERROR [Wallet module build]', text, 'white', 'bgRed')

// get the root project dir assuming this config file is in subdir (eg. ./build-utils) of the main project dir
const rootProjectDir = path.join(__dirname, '..')

function getWallet () {
  const walletErrorMsg = `Please specify one of the supported wallets (${walletDirs.join(', ')}). Eg: --wallet=${walletDirs[0].toLowerCase()}`
  if (argv.wallet) {
    try {
      const i = walletDirs.findIndex(w => w.toLowerCase() === argv.wallet.toLowerCase())
      if (i === -1) {
        logError(`Unknown wallet specified: '${argv.wallet}'. ${walletErrorMsg}`)
        process.exit(1)
      }
      process.env.VUE_APP_WALLET = walletDirs[i]
      logInfo(`Selected wallet: ${walletDirs[i]} `)
    } catch (error) {
      logError(`Error parsing wallet parameter from command line. ${walletErrorMsg}`)
      console.log(error)
      process.exit(1)
    }
  } else {
    // check if wallet is specified in env variable
    if (!process.env.VUE_APP_WALLET) {
      logInfo(`Wallet not specified in 'process.env.VUE_APP_WALLET'. Using default value: --wallet=${walletDirs[0]}`)
      process.env.VUE_APP_WALLET = walletDirs[0]
    } else {
      logInfo(`Selected wallet (from env variable VUE_APP_WALLET): ${process.env.VUE_APP_WALLET}`)
    }
  }

  // const walletResolvePath = path.resolve(__dirname, 'src/wallets/' + process.env.VUE_APP_WALLET)
  const walletResolvePath = path.resolve(rootProjectDir, 'src/wallets/' + process.env.VUE_APP_WALLET)
  if (!fs.existsSync(walletResolvePath) && fs.dirent.isDirectory(walletResolvePath)) {
    logError(`The wallet module directory doesn't exist: ${walletResolvePath}`)
    process.exit(1)
  }

  return {
    resolve: {
      alias: {
        '#wallet#': walletResolvePath
      }
    }
  }
}

function walletWebpackConfig (config) {
  return getWallet()
}

module.exports = {
  walletWebpackConfig
}

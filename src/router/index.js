import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import walletRoutes from '#wallet#/routes'

Vue.use(VueRouter)

let routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

routes = routes.concat(walletRoutes)
// console.log('routes:', routes)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// handle NavigationDuplicated error so it doesn't show error in console
// this error happens when application tries to navigate to the same route it's already on
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = async function push (location) {
  try {
    return await originalPush.call(this, location)
  } catch (error) {
    if (!(error && error.name === 'NavigationDuplicated')) {
      throw error
    }
  }
}

export default router

import api from '../api'

export default {
  namespaced: true,

  state: {
    loading: null,
    balance: 25.00,
    personalDetails: null
  },

  mutations: {
    setPersonalDetails (state, value) {
      state.personalDetails = value
    },
    setLoading (state, value) {
      state.loading = value
    }
  },
  getters: {
    isLoggedIn (state) {
      return !!state.personalDetails
    },

    firstName (state) {
      return state.personalDetails && state.personalDetails.FirstName
    },

    lastName (state) {
      return state.personalDetails && state.personalDetails.LastName
    }
  },
  actions: {
    async tryAutoLogin ({ dispatch }) {
      if (await api.tryAutoLogin()) {
        await dispatch('getPersonalDetails')
      }
    },

    async getPersonalDetails ({ commit }) {
      const personalDetails = await api.getPersonalDetails()
      commit('setPersonalDetails', personalDetails)
    },

    async login ({ commit, dispatch }, { username, password }) {
      try {
        commit('setLoading', true)
        const isLoggedIn = await api.login(username, password)
        if (isLoggedIn) {
          await dispatch('getPersonalDetails')
          return true
        } else {
          return false
        }
      } catch (error) {
        console.error('Wallet vuex module error: login exception', error)
        throw error // give chance to the caller to do it's own exception handling
      } finally {
        commit('setLoading', false)
      }
    },

    async logout ({ commit }) {
      try {
        commit('setLoading', true)
        await api.logout()
        commit('setPersonalDetails', null)
      } finally {
        commit('setLoading', false)
      }
    }
  }
}

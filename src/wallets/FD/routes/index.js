import Login from '../components/Login.vue'
import AboutWallet from '../views/AboutWallet.vue'

const routes = [
  {
    path: '/account/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/wallet/about',
    name: 'AboutWallet',
    component: AboutWallet
  }
]

export default routes

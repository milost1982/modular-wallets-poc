// FD api mock
function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}
async function simulateServerDelay () {
  await sleep(500)
}
async function login (username, password) {
  await simulateServerDelay(1000)
  if (username === 'user1@test.com' && password === 'pass1') {
    localStorage.setItem('isLoggedIn', true)
    return true
  }
  return false
}

async function getPersonalDetails () {
  return {
    Title: 'Mr',
    FirstName: 'David',
    MiddleNames: null,
    LastName: 'Miller',
    PrimaryEmail: null,
    HomePhone: null,
    MobilePhone: 'none',
    IDMMCountry: 'US',
    IDMMCountryCitizenship: null,
    IDDCLanguage: 'US',
    StreetAddress: 'none',
    City: null,
    PostCode: null,
    CountyOrStateOrProvince: null,
    FullName: 'User  2',
    UserName: 'user2',
    DateOfBirth: '1-1-0001 0-0-0',
    PlaceOfBirth: null,
    Profession: null,
    AffiliateId: null,
    IsVerificationRequired: true,
    IDMMCustAcqSource: null,
    IDMMGender: 'M',
    IDDCSecretQuestion: null,
    SecretAnswer: null,
    IDDCSecretQuestion2: null,
    SecretAnswer2: null,
    IVRPin: null,
    IDMMRegion: null,
    IDMMMunicipality: null,
    DateRegistered: '14-9-2020 8-33-35',
    IsDCUserActive: true,
    Salutation: null
  }
}

async function logout () {
  await sleep(1500)
  localStorage.removeItem('isLoggedIn')
  return true
}

async function tryAutoLogin () {
  return !!localStorage.getItem('isLoggedIn')
}

export default {
  login,
  tryAutoLogin,
  logout,
  getPersonalDetails
}

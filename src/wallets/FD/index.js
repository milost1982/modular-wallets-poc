import './assets/css/main.css'
import './assets/css/sb.styl'

import router from '@/router'
import walletRoutes from './routes'
import { registerGlobalComponents } from './components/global.js'
import { eventBus } from '@/services/eventBus' // service from the app we want to use in wallet module, import with absolute path
import store from '@/store'

console.log('Wallet in use: FanDuel Wallet')

// helper function to import all files from specific directory
function importAll (r) {
  return r.keys().map(r)
}
// tell webpack to import all images from local wallet assets dir
importAll(require.context('./assets/images', false, /\.(png|jpe?g|svg)$/))

registerGlobalComponents()

function initializeWallet () {
  console.log('Initializing FD wallet')
}

eventBus.$on('login-success', () => router.push('/'))

function login () {
  console.log('FD login')
  router.push({ name: 'Login' })
}

async function logout () {
  console.log('FD logout')
  await store.dispatch('wallet/logout', null, { root: true })
  eventBus.$emit('user-logged-out')
  router.push('/')
}

function register () {
  console.log('FD register')
  window.location.href = 'https://account.nj.sportsbook.qa.fndl.dev/join'
}

async function getCustomerContext () {
  console.log('FD getCustomerContext')
  await store.dispatch('wallet/getPersonalDetails', null, { root: true })
}

async function tryAutoLogin () {
  await store.dispatch('wallet/tryAutoLogin', null, { root: true })
}
/**
 * Object with links that lead to wallet pages
 * @type {object}
 */
function getNavItems () {
  console.log('FD getNavigationItems')
  return [
    {
      name: 'Responsible Gaming',
      href: 'https://myaccount.qa.gameaccount.com/responsible-gaming.shtml',
      icon: 'main-nav-icon icon-rg',
      iconType: 'css-class'
    },
    {
      name: 'Active Bets', // name to display for this item
      href: '/sports/history/active',
      icon: 'main-nav-icon icon-active-betslip',
      iconType: 'css-class'
    },
    {
      name: 'Transaction History',
      href: 'https://myaccount.qa.gameaccount.com/account-history.shtml',
      icon: 'main-nav-icon icon-betting-history',
      iconType: 'css-class'
    },
    {
      name: 'Account Summary',
      href: 'https://myaccount.qa.gameaccount.com/account-summary.do',
      icon: 'main-nav-icon icon-user',
      iconType: 'css-class'
    },
    {
      name: 'Patron Protection',
      href: '/wallet/lock',
      icon: 'main-nav-icon icon-lock',
      iconType: 'css-class'
    },
    {
      name: 'Contact Support',
      href: 'https://support.fanduel.com/s/',
      icon: 'main-nav-icon icon-chat',
      iconType: 'css-class'
    },
    {
      name: 'Change Password',
      href: '/wallet/password',
      icon: 'main-nav-icon icon-settings',
      iconType: 'css-class'
    },
    {
      name: 'FAQ',
      href: '/wallet/faq',
      icon: 'main-nav-icon icon-information',
      iconType: 'css-class'
    },
    {
      isSeparator: true
    },
    {
      name: 'About wallet',
      href: '/wallet/about',
      icon: 'mdi-wallet',
      iconType: 'css-class',
      publicItem: true // this item doesn't require logged-in user
    }
  ]
}

function getRoutes () {
  return walletRoutes
}
const name = 'FD Wallet'

// JSDoc
/**
 * @typedef Wallet interface that all wallets must export
 * @type {object}
 * @property {string} name - a wallet name
 * @property {function} initializeWallet - function that initializes wallet (eg. give chance to wallet module to perform any tasks it might require)
 * @property {function} login - shows login dialog
 * @property {function} logout - logs user out
 * @property {function} register - navigates to the registration page
 * @property {function} getCustomerContext - returns customer context object from the backend
 * @property {function} getNavItems - returns list of items to build navigation menu
 * @property {function} getRoutes - returns routes for the wallet module
 */
export default {
  name,
  initializeWallet,
  tryAutoLogin,
  login,
  logout,
  register,
  getCustomerContext,
  getNavItems,
  getRoutes
}

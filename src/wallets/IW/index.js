import './assets/css/main.css'
import './assets/css/sb.styl'

import router from '@/router'
import walletRoutes from './routes'
import { registerGlobalComponents } from './components/global.js'
import { eventBus } from '@/services/eventBus' // service from the app, import with absolute path
import store from '@/store'

// import './assets/images/w2g.svg' // import all files from this folder
// import './assets/images' // not going to work

console.log('Wallet in use: Integrated Wallet') // string to check if it exists in bundle

// helper function to import all files from specific directory
function importAll (r) {
  return r.keys().map(r)
}
// tell webpack to import all images from local wallet assets dir
importAll(require.context('./assets/images', false, /\.(png|jpe?g|svg)$/))

registerGlobalComponents()

function initializeWallet () {
  console.log('Initializing IW wallet')
}

function login () {
  console.log('IW login')
  eventBus.$emit('show-login')
}

async function logout () {
  console.log('IW logout')
  // await api.logout()
  // store.dispatch('')
  await store.dispatch('wallet/logout', null, { root: true })
  eventBus.$emit('user-logged-out')
}

function register () {
  console.log('IW register')
  router.push({ name: 'Register' })
}

function getCustomerContext () {
  console.log('IW getCustomerContext')
}

async function tryAutoLogin () {
  await store.dispatch('wallet/tryAutoLogin', null, { root: true })
}
/**
 * Object with links that lead to wallet pages
 * @type {object}
 */
function getNavItems () {
  console.log('IW getNavigationItems')
  return [
    {
      name: 'Account Summary', // name to display for this item
      href: '/account/account-summary',
      icon: 'icon-acc-summary',
      iconType: 'css-class'
    },
    {
      name: 'Account Settings',
      href: '/account/settings',
      icon: 'icon-acc-settings',
      iconType: 'css-class'
    },
    {
      name: 'Self-Exclusion and Limits',
      href: '/account/responsible-gaming',
      icon: 'icon-acc-rg-stop',
      iconType: 'css-class'
    },
    {
      name: 'Responsible Gaming',
      href: '/account/responsible-gaming',
      icon: 'https://iw.dev.igt.rs/media/static/brand-img/sb/rg-logo-nj.svg',
      iconType: 'url'
    },
    {
      isSeparator: true
    },
    {
      name: 'Transaction history',
      href: '/account/transactions',
      icon: 'icon-tx-history',
      iconType: 'css-class'
    },
    {
      name: 'Active bets',
      href: '/account/bethistory/active',
      icon: 'icon-active-bets',
      iconType: 'css-class'
    },
    {
      name: 'Settled Bets',
      href: '/account/bethistory/settled',
      icon: 'icon-settled-bets',
      iconType: 'css-class'
    },
    {
      isSeparator: true
    },
    {
      name: 'W2G Forms',
      href: '/account/w2gtaxforms',
      icon: 'icon-w2g',
      iconType: 'css-class'
    },
    {
      isSeparator: true
    },
    {
      name: 'About wallet',
      href: '/wallet/about',
      icon: 'mdi-wallet',
      iconType: 'css-class',
      publicItem: true // this item doesn't require logged-in user
    }
  ]
}

function getRoutes () {
  return walletRoutes
}
const name = 'Integrated Wallet'

// JSDoc
/**
 * @typedef Wallet interface that all wallets must export
 * @type {object}
 * @property {string} name - a wallet name
 * @property {function} initializeWallet - function that initializes wallet (eg. give chance to wallet module to perform any tasks it might require)
 * @property {function} login - shows login dialog
 * @property {function} logout - logs user out
 * @property {function} register - navigates to the registration page
 * @property {function} getCustomerContext - returns customer context object from the backend
 * @property {function} getNavItems - returns list of items to build navigation menu
 * @property {function} getRoutes - returns routes for the wallet module
 */
export default {
  name,
  initializeWallet,
  tryAutoLogin,
  login,
  logout,
  register,
  getCustomerContext,
  getNavItems,
  getRoutes
}

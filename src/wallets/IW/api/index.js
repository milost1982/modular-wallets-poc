// IW api mock
function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}
async function simulateServerDelay () {
  await sleep(500)
}
async function login (username, password) {
  await simulateServerDelay(1000)
  if (username === 'user1@test.com' && password === 'pass1') {
    localStorage.setItem('isLoggedIn', true)
    return true
  }
  return false
}

async function getCustomerContext () {
  return {
    Name: [
      'John',
      'Smith'
    ],
    User: 'user1@test.com',
    ActionBet: false,
    ActionBetReason: 'SUSPENSION',
    ActionClose: false,
    ActionDeposit: false,
    ActionWithdraw: false,
    ActionUpdate: true,
    Alerts: [
      {
        Key: 'AID',
        Text: 'Manual age and identity verification',
        Status: 'NOTSENT'
      },
      {
        Key: 'AVS',
        Text: 'Personal details - Age verification service',
        Status: 'FAILED'
      },
      {
        Key: 'EML',
        Text: 'E-mail verification',
        Status: 'SENT'
      },
      {
        Key: 'MTL',
        Text: 'Mobile telephone verification',
        Status: 'SENT'
      },
      {
        Key: 'SSN',
        Text: 'SSN verification',
        Status: null
      }
    ],
    Balances: [
      {
        Key: 'CREDIT',
        Amount: 0.0,
        Locked: 0.0,
        Promo: 100.0,
        Withdrawal: 0.0,
        Trading: 0.0,
        AccountNumber: 'W2DEV213244',
        Bets: 0.0,
        Winnings: 0.0,
        Total: 0.0,
        NoOfDeposits: 0,
        NoOfBets: 0,
        NoOfWithdrawals: 0
      }
    ],
    Logins: [
      '2021-10-08T07:52:19Z',
      '2021-09-30T04:56:39Z'
    ],
    Verified: false,
    TimeLimitSeconds: 3599.0,
    Flags: null
  }
}

async function logout () {
  await sleep(1500)
  localStorage.removeItem('isLoggedIn')
  return true
}

async function tryAutoLogin () {
  return !!localStorage.getItem('isLoggedIn')
}

export default {
  login,
  tryAutoLogin,
  logout,
  getCustomerContext
}

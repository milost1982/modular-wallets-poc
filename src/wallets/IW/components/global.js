// register all global wallet components
// all wallets should have the same global components
//

import Vue from 'vue'
import SampleWalletComponent from './SampleWalletComponent'

function registerGlobalComponents () {
  Vue.component('w-sample-wallet-component', SampleWalletComponent) // global registration in Vue 2

  // for Vue3: https://v3.vuejs.org/guide/component-registration.html#component-names
}

export {
  registerGlobalComponents
}

import Register from '../components/Register.vue'
import AboutWallet from '../views/AboutWallet.vue'

const routes = [
  {
    path: '/account/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/wallet/about',
    name: 'AboutWallet',
    component: AboutWallet
  }
]

export default routes

import api from '../api'

export default {
  namespaced: true,

  state: {
    loading: null,
    balance: 25.00,
    customerContext: null
  },

  mutations: {
    setCustomerContext (state, value) {
      state.customerContext = value
    },
    setLoading (state, value) {
      state.loading = value
    }
  },
  getters: {
    isLoggedIn (state) {
      return !!state.customerContext
    },

    firstName (state) {
      return state.customerContext && state.customerContext.Name[0]
    },
    lastName (state) {
      return state.customerContext && state.customerContext.Name[1]
    }
  },
  actions: {
    async tryAutoLogin ({ dispatch }) {
      if (await api.tryAutoLogin()) {
        await dispatch('getCustomerContext')
      }
    },

    async getCustomerContext ({ commit }) {
      const customerContext = await api.getCustomerContext()
      commit('setCustomerContext', customerContext)
    },

    async login ({ commit, dispatch }, { username, password }) {
      try {
        commit('setLoading', true)
        const isLoggedIn = await api.login(username, password)
        if (isLoggedIn) {
          await dispatch('getCustomerContext')
          return true
        } else {
          return false
        }
      } catch (error) {
        console.error('Wallet vuex module error: login exception', error)
        throw error // give chance to the caller to do it's own exception handling
      } finally {
        commit('setLoading', false)
      }
    },

    async logout ({ commit }) {
      try {
        commit('setLoading', true)
        await api.logout()
        commit('setCustomerContext', null)
      } finally {
        commit('setLoading', false)
      }
    }
  }
}

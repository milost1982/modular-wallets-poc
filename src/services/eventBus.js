// eventBus.js
import emitter from 'tiny-emitter/instance'

export const eventBus = {
  $on: (...args) => emitter.on(...args),
  $once: (...args) => emitter.once(...args),
  $off: (...args) => emitter.off(...args),
  $emit: (...args) => emitter.emit(...args)
}

// events for communicating between app and wallet module
export const walletEvents = {
  showLogin: 'show-login'
}

// events for communicating with app components
export const appEvents = {
  showModalDialog: 'show-modal-dialog'
}

NPM Packages used:

Yargs helps you build interactive command line tools, by parsing arguments and generating an elegant user interface.
npm i yargs --save-dev



Webpack configuration 
This is done in vue.config.js (https://cli.vuejs.org/config/#vue-config-js)
Create vue.config.js to add custom webpack configuration (https://cli.vuejs.org/config/#configurewebpack)


Communication between wallet and app via event bus pattern (aka. event aggregator pattern or publisher-subscriber pattern)
As recommended in the docs:
https://v3.vuejs.org/guide/migration/events-api.html#event-bus


Think about application as the host (or a container) of a wallet module. 
So wallet knows it's hosting environment, but application doesn't know which exact wallet is hosting but only that it hosts some wallet.

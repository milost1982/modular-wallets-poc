const { walletWebpackConfig } = require('./build-utils/walletWebpackConfig')
const { merge } = require('webpack-merge')

module.exports = {
  lintOnSave: process.env.NODE_ENV === 'production' ? 'error' : 'warning',
  transpileDependencies: [
    'vuetify'
  ],

  configureWebpack: config => {
    return merge(walletWebpackConfig(config))
  }
}
